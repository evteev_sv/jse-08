package com.nlmk.evteev.tm;

import com.nlmk.evteev.tm.dao.ProjectDAO;
import com.nlmk.evteev.tm.dao.TaskDAO;
import com.nlmk.evteev.tm.entity.Project;
import com.nlmk.evteev.tm.entity.Task;

import java.util.Scanner;

import static com.nlmk.evteev.tm.constant.TerminalConst.*;

/**
 * Основной класс
 */
public class Application {

    private static final ProjectDAO projectDAO = new ProjectDAO();
    private static final TaskDAO taskDAO = new TaskDAO();
    private static final Scanner scanner = new Scanner(System.in);

    static {
        projectDAO.create("PROJECT DEMO 1");
        projectDAO.create("PROJECT DEMO 2");
        taskDAO.create("DEMO TASK 1");
        taskDAO.create("DEMO TASK 2");
    }

    /**
     * Точка входа
     *
     * @param args дополнительные аргументы запуска
     */
    public static void main(String[] args) {
        displayWelcome();
        run(args);
        String command = "";
        while (!CMD_EXIT.equals(command)) {
            command = scanner.nextLine();
            run(command);
            System.out.println();
        }
    }

    /**
     * Основной метод исполнения
     *
     * @param args дополнительные параметры запуска
     */
    private static void run(final String[] args) {
        if (args == null || args.length < 1) return;
        final String param = args[0];
        run(param);
    }

    /**
     * Обработка консольного ввода
     *
     * @param cmdString команда с консоли
     * @return код выполнения
     */
    private static int run(final String cmdString) {
        if (cmdString == null || cmdString.isEmpty()) return -1;
        switch (cmdString) {
            case CMD_VERSION:
                return displayVersion();
            case CMD_HELP:
                return displayHelp();
            case CMD_ABOUT:
                return displayAbout();
            case CMD_EXIT:
                displayExit();
            case PROJECT_CREATE:
                return createProject();
            case PROJECT_CLEAR:
                return clearProject();
            case PROJECT_LIST:
                return listProject();
            case PROJECT_VIEW:
                return viewProjectByIndex();
            case PROJECT_REMOVE_BY_ID:
                return removeProjectById();
            case PROJECT_REMOVE_BY_NAME:
                return removeProjectByName();
            case PROJECT_REMOVE_BY_INDEX:
                return removeProjectByIndex();
            case PROJECT_UPDATE_BY_INDEX:
                return updateProjectByIndex();
            case PROJECT_UPDATE_BY_ID:
                return updateProjectById();
            case TASK_CREATE:
                return createTask();
            case TASK_CLEAR:
                return clearTask();
            case TASK_LIST:
                return listTask();
            case TASK_VIEW:
                return viewTaskByIndex();
            case TASK_REMOVE_BY_ID:
                return removeTaskById();
            case TASK_REMOVE_BY_NAME:
                return removeTaskByName();
            case TASK_REMOVE_BY_INDEX:
                return removeTaskByIndex();
            case TASK_UPDATE_BY_ID:
                return updateTaskById();
            case TASK_UPDATE_BY_INDEX:
                return updateTaskByIndex();
            default:
                return displayError();
        }
    }

    /**
     * Показ сообщения об ошибке
     */
    private static int displayError() {
        System.out.println("Неподдерживаемый аргумент. наберите команду " +
            "help для получения списка доступных команд...");
        return -1;
    }

    /**
     * Показ сведений о ключах
     */
    private static int displayHelp() {
        System.out.println("version - Информация о версии.");
        System.out.println("about - Информация о разработчике.");
        System.out.println("help - Информация о доступных командах.");
        System.out.println("exit - Завершение работы приложения");
        System.out.println("proj-create - Создание проекта");
        System.out.println("proj-clear - Очистка проектов");
        System.out.println("proj-list - Вывод списка проектов");
        System.out.println("proj-view - Просмотр проектов");
        System.out.println("proj-remove-by-name - Удаление проекта по имени");
        System.out.println("proj-remove-by-id - Удаление проекта по коду");
        System.out.println("proj-remove-by-index - Удаление проекта по индексу");
        System.out.println("proj-update-by-index - Изменение проекта по индексу");
        System.out.println("proj-update-by-id - Изменение проекта по коду");
        System.out.println("task-create - Создание задачи");
        System.out.println("task-clear - Очистка задачи");
        System.out.println("task-list - Вывод списка задач");
        System.out.println("task-view - Просмотр задачи");
        System.out.println("task-remove-by-id - Удаление задачи по коду");
        System.out.println("task-remove-by-name - Удаление задачи по имени");
        System.out.println("task-remove-by-index - Удаление задачи по индексу");
        System.out.println("task-update-by-index - Изменение задачи по индексу");
        System.out.println("task-update-by-id - Изменение задачи по коду");
        return 0;
    }

    /**
     * Показ сведений о версиях
     */
    private static int displayVersion() {
        System.out.println("1.0.5");
        return 0;
    }

    /**
     * Показ сведений об авторе
     */
    private static int displayAbout() {
        System.out.println("Author: Sergey Evteev");
        System.out.println("e-mail: sergey@evteev.ru");
        return 0;
    }

    /**
     * Показ приветствия
     */
    private static void displayWelcome() {
        System.out.println("** WELCOME TO TASK MANAGER **");
    }

    /**
     * Стандартный выход
     */
    private static void displayExit() {
        System.out.println("Получена команда завершения работы...");
        System.exit(0);
    }

    /**
     * Создание проекта
     *
     * @return код выполнения
     */
    private static int createProject() {
        System.out.println("[CREATE PROJECT]");
        System.out.println("Укажите имя проекта: ");
        final String lv_name = scanner.nextLine();
        projectDAO.create(lv_name);
        System.out.println("[OK]");
        return 0;
    }

    /**
     * Очистка проекта
     *
     * @return код выполнения
     */
    private static int clearProject() {
        System.out.println("[CLEAR PROJECT]");
        projectDAO.clear();
        System.out.println("[OK]");
        return 0;
    }

    /**
     * Список проектов
     *
     * @return код выполнения
     */
    private static int listProject() {
        System.out.println("[LIST PROJECT]");
        System.out.println(projectDAO.findAll());
        System.out.println("[OK]");
        return 0;
    }

    /**
     * Создание задачи
     *
     * @return код выполнения
     */
    private static int createTask() {
        System.out.println("[CREATE TASK]");
        System.out.println("Укажите имя задачи: ");
        final String lv_name = scanner.nextLine();
        taskDAO.create(lv_name);
        System.out.println("[OK]");
        return 0;
    }

    /**
     * Очистка задачи
     *
     * @return код выполнения
     */
    private static int clearTask() {
        System.out.println("[CLEAR TASK]");
        taskDAO.clear();
        System.out.println("[OK]");
        return 0;
    }

    /**
     * Список задач
     *
     * @return код выполнения
     */
    private static int listTask() {
        System.out.println("[LIST PROJECT]");
        System.out.println(taskDAO.findAll());
        System.out.println("[OK]");
        return 0;
    }

    /**
     * Просмотр проекта в консоли
     *
     * @param project проект {@link com.nlmk.evteev.tm.entity.Project}
     */
    private static void viewProject(final Project project) {
        if (project == null) return;
        System.out.println("[View Project]");
        System.out.println("ID: " + project.getId());
        System.out.println("NAME: " + project.getName());
        System.out.println("DESCRIPTION: " + project.getDescription());
        System.out.println("[OK]");
    }

    /**
     * Просмотр задачи в консоли
     *
     * @param task задача {@link com.nlmk.evteev.tm.entity.Task}
     */
    private static void viewTask(final Task task) {
        if (task == null) return;
        System.out.println("[View Task]");
        System.out.println("ID: " + task.getId());
        System.out.println("NAME: " + task.getName());
        System.out.println("DESCRIPTION: " + task.getDescription());
        System.out.println("[OK]");
    }

    /**
     * Просмотр задачи по индексу
     *
     * @return код исполнения
     */
    private static int viewTaskByIndex() {
        System.out.println("Введите индекс задачи: ");
        final int index = scanner.nextInt() - 1;
        final Task task = taskDAO.findByIndex(index);
        viewTask(task);
        return 0;
    }

    /**
     * Просмотр проекта по индексу
     *
     * @return код исполнения
     */
    private static int viewProjectByIndex() {
        System.out.println("Введите индекс проекта: ");
        final int index = scanner.nextInt() - 1;
        final Project project = projectDAO.findByIndex(index);
        viewProject(project);
        return 0;
    }

    /**
     * Удаление задачи по названию
     *
     * @return код исполнения
     */
    private static int removeTaskByName() {
        System.out.println("[Remove task by name]");
        System.out.println("Введите имя задачи: ");
        final String name = scanner.nextLine();
        final Task task = taskDAO.removeByName(name);
        if (task == null) {
            System.out.println("[FAIL]");
        } else {
            System.out.println("[OK]");
        }
        return 0;
    }

    /**
     * Удаление задачи по коду
     *
     * @return код исполнения
     */
    private static int removeTaskById() {
        System.out.println("[Remove task by id]");
        System.out.println("Введите ID задачи: ");
        final Long vId = scanner.nextLong();
        final Task task = taskDAO.removeById(vId);
        if (task == null) {
            System.out.println("[FAIL]");
        } else {
            System.out.println("[OK]");
        }
        return 0;
    }

    /**
     * Удаление задачи по индексу
     *
     * @return код исполнения
     */
    private static int removeTaskByIndex() {
        System.out.println("[Remove task by index]");
        System.out.println("Введите индекс задачи: ");
        final Integer vId = scanner.nextInt();
        final Task task = taskDAO.removeByIndex(vId);
        if (task == null) {
            System.out.println("[FAIL]");
        } else {
            System.out.println("[OK]");
        }
        return 0;
    }

    /**
     * Удаление проекта по имени
     *
     * @return код исполнения
     */
    private static int removeProjectByName() {
        System.out.println("[Remove project by name]");
        System.out.println("Введите имя проекта: ");
        final String name = scanner.nextLine();
        Project project = projectDAO.removeByName(name);
        if (project == null) {
            System.out.println("[FAIL]");
        } else {
            System.out.println("[OK]");
        }
        return 0;
    }

    /**
     * Удаление проекта по коду
     *
     * @return код исполнения
     */
    private static int removeProjectById() {
        System.out.println("[Remove project by id]");
        System.out.println("Введите ID проекта: ");
        final Long vId = scanner.nextLong();
        Project project = projectDAO.removeById(vId);
        if (project == null) {
            System.out.println("[FAIL]");
        } else {
            System.out.println("[OK]");
        }
        return 0;
    }

    /**
     * Удаление проекта по индексу
     *
     * @return код исполнения
     */
    private static int removeProjectByIndex() {
        System.out.println("[Remove project by index]");
        System.out.println("Введите индекс проекта: ");
        final int vId = scanner.nextInt();
        Project project = projectDAO.removeByIndex(vId);
        if (project == null) {
            System.out.println("[FAIL]");
        } else {
            System.out.println("[OK]");
        }
        return 0;
    }

    /**
     * Изменение проекта по индексу
     *
     * @return код исполнения
     */
    private static int updateProjectByIndex() {
        System.out.println("[Update project by index]");
        System.out.println("Введите индекс проекта: ");
        final int vId = Integer.parseInt(scanner.nextLine()) - 1;
        final Project project = projectDAO.findByIndex(vId);
        if (project == null) {
            System.out.println("[FAIL]");
            return 0;
        }
        System.out.println("Введите новое имя проекта: ");
        final String name = scanner.nextLine();
        System.out.println("Введите новое описание проекта: ");
        final String description = scanner.nextLine();
        if (projectDAO.update(project.getId(), name, description) != null) {
            System.out.println("[OK]");
        } else {
            System.out.println("[FAIL]");
        }
        return 0;
    }

    /**
     * Изменение проекта по коду
     *
     * @return код исполнения
     */
    private static int updateProjectById() {
        System.out.println("[Update project by id]");
        System.out.println("Введите код проекта: ");
        final Long vId = Long.getLong(scanner.nextLine());
        System.out.println("Введите новое имя проекта: ");
        final String name = scanner.nextLine();
        System.out.println("Введите новое описание проекта: ");
        final String description = scanner.nextLine();
        if (projectDAO.update(vId, name, description) != null) {
            System.out.println("[OK]");
        } else {
            System.out.println("[FAIL]");
        }
        return 0;
    }

    /**
     * Изменение задачи по индексу
     *
     * @return код исполнения
     */
    private static int updateTaskByIndex() {
        System.out.println("[Update task by index]");
        System.out.println("Введите индекс задачи: ");
        final int vId = Integer.parseInt(scanner.nextLine()) - 1;
        final Task task = taskDAO.findByIndex(vId);
        if (task == null) {
            System.out.println("[FAIL]");
            return 0;
        }
        System.out.println("Введите новое название задачи: ");
        final String name = scanner.nextLine();
        System.out.println("Введите новое описание задачи: ");
        final String description = scanner.nextLine();
        if (taskDAO.update(task.getId(), name, description) != null) {
            System.out.println("[OK]");
        } else {
            System.out.println("[FAIL]");
        }
        return 0;
    }

    /**
     * Изменение задачи по коду
     *
     * @return код исполнения
     */
    private static int updateTaskById() {
        System.out.println("[Update task by id]");
        System.out.println("Введите код задачи: ");
        final Long vId = Long.getLong(scanner.nextLine());
        System.out.println("Введите новое название задачи: ");
        final String name = scanner.nextLine();
        System.out.println("Введите новое описание задачи: ");
        final String description = scanner.nextLine();
        if (taskDAO.update(vId, name, description) != null) {
            System.out.println("[OK]");
        } else {
            System.out.println("[FAIL]");
        }
        return 0;
    }

}
