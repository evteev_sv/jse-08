package com.nlmk.evteev.tm.entity;

/**
 * Класс, описывающий задачи
 */
public class Task {

    //<editor-fold defaultstate="collapsed" desc="Объявление переменных">
    private Long id = System.nanoTime();
    private String name = "";
    private String description = "";
    //</editor-fold>

    /**
     * Конструктор по умолчанию
     */
    public Task() {
    }

    /**
     * Конструктор класса
     *
     * @param pName название задачи
     */
    public Task(String pName) {
        name = pName;
    }

    //<editor-fold defaultstate="collapsed" desc="Методы установки/получения полей">
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
    //</editor-fold>

    @Override
    public String toString() {
        return "Task {id=" + id + ", name=" + name + "}";
    }

}
