package com.nlmk.evteev.tm.dao;

import com.nlmk.evteev.tm.entity.Task;

import java.util.ArrayList;
import java.util.List;

/**
 * Класс для работы с объектом задачи
 * {@link Task}
 */
public class TaskDAO {

    private final List<Task> tasks = new ArrayList<>();

    /**
     * Создание задачи и добавление ее в список задач
     *
     * @param pName имя задачи
     * @return объект типа {@link Task}
     */
    public Task create(final String pName) {
        final Task task = new Task(pName);
        tasks.add(task);
        return task;
    }

    /**
     * Очистка списка задач
     */
    public void clear() {
        tasks.clear();
    }

    /**
     * Возвращает список задач
     *
     * @return список задач {@link java.util.List}
     */
    public List<Task> findAll() {
        return tasks;
    }

    /**
     * Поиск задачи по индексу
     *
     * @param index индекс задачи
     * @return задача {@link com.nlmk.evteev.tm.entity.Task}
     */
    public Task findByIndex(int index) {
        if (index < 0 || index > tasks.size())
            return null;
        return tasks.get(index);
    }

    /**
     * Поиск задачи по наименованию
     *
     * @param name наименование задачи
     * @return задача {@link Task}
     */
    public Task findByName(final String name) {
        if (name == null || name.isEmpty())
            return null;
        for (Task task : tasks) {
            if (task.getName().equals(name))
                return task;
        }
        return null;
    }

    /**
     * Поиск задачи по коду
     *
     * @param id колд задачи
     * @return задача {@link Task}
     */
    public Task findById(final Long id) {
        if (id == null)
            return null;
        for (Task task : tasks) {
            if (task.getId().equals(id))
                return task;
        }
        return null;
    }

    /**
     * Удаление задачи по коду
     *
     * @param id код задачи
     * @return удаленная задача {@link Task}
     */
    public Task removeById(final Long id) {
        final Task task = findById(id);
        if (task == null) return null;
        tasks.remove(task);
        return task;
    }

    /**
     * Удаление задачи по наименованию
     *
     * @param name наименование задачи
     * @return удаленная задача {@link Task}
     */
    public Task removeByName(final String name) {
        final Task task = findByName(name);
        if (task == null) return null;
        tasks.remove(task);
        return task;
    }

    /**
     * Удаление задачи по индексу
     *
     * @param index индекс задачи
     * @return удаленная задача {@link Task}
     */
    public Task removeByIndex(Integer index) {
        final Task task = findByIndex(index);
        if (task == null) return null;
        tasks.remove(task);
        return task;
    }

    /**
     * Изменение задачи
     *
     * @param id          код задачи
     * @param name        наименование задачи
     * @param description описание задачи
     * @return измененная задача {@link Task}
     */
    public Task update(final Long id, final String name, final String description) {
        Task task = findById(id);
        if (task == null)
            return null;
        task.setName(name);
        task.setDescription(description);
        return task;
    }

}
