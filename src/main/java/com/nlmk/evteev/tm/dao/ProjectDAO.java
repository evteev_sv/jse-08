package com.nlmk.evteev.tm.dao;

import com.nlmk.evteev.tm.entity.Project;

import java.util.ArrayList;
import java.util.List;

/**
 * Класс для работы с объектом проект
 * {@link com.nlmk.evteev.tm.entity.Project}
 */
public class ProjectDAO {

    private List<Project> projects = new ArrayList<>();

    /**
     * Создание проекта и добавление его в список проектов
     *
     * @param pName имя проекта
     * @return объект типа {@link com.nlmk.evteev.tm.entity.Project}
     */
    public Project create(final String pName) {
        final Project project = new Project(pName);
        projects.add(project);
        return project;
    }

    /**
     * Очистка списка проектов
     */
    public void clear() {
        projects.clear();
    }

    /**
     * Возвращает список проектов
     *
     * @return список проектов {@link java.util.List}
     */
    public List<Project> findAll() {
        return projects;
    }

    /**
     * Поиск проекта по индексу
     *
     * @param index индекс проекта
     * @return проект {@link com.nlmk.evteev.tm.entity.Project}
     */
    public Project findByIndex(final int index) {
        if (index < 0 || index > projects.size())
            return null;
        return projects.get(index);
    }

    /**
     * Поиск проекта по наименованию
     *
     * @param name наименование проекта
     * @return проект {@link com.nlmk.evteev.tm.entity.Project}
     */
    public Project findByName(final String name) {
        if (name == null || name.isEmpty())
            return null;
        for (Project project : projects) {
            if (project.getName().equals(name))
                return project;
        }
        return null;
    }

    /**
     * Поиск проекта по коду
     *
     * @param id код проекта
     * @return проект {@link com.nlmk.evteev.tm.entity.Project}
     */
    public Project findById(final Long id) {
        if (id == null)
            return null;
        for (Project project : projects) {
            if (project.getId().equals(id))
                return project;
        }
        return null;
    }

    /**
     * Удаление проекта по коду
     *
     * @param id код проекта
     * @return проект, который был удален {@link com.nlmk.evteev.tm.entity.Project}
     */
    public Project removeById(final Long id) {
        final Project project = findById(id);
        if (project == null) return null;
        projects.remove(project);
        return project;
    }

    /**
     * Удален епроекта по индексу
     *
     * @param index индекс проекта
     * @return проект, который был удален {@link com.nlmk.evteev.tm.entity.Project}
     */
    public Project removeByIndex(final int index) {
        final Project project = findByIndex(index);
        if (project == null) return null;
        projects.remove(project);
        return project;
    }

    /**
     * Удаление проекта по названию.
     *
     * @param name название проекта
     * @return проект, который был удален {@link com.nlmk.evteev.tm.entity.Project}
     */
    public Project removeByName(final String name) {
        final Project project = findByName(name);
        if (project == null) return null;
        projects.remove(project);
        return project;
    }

    /**
     * Изменение проекта
     *
     * @param id          код проекта
     * @param name        наименование проекта
     * @param description описание проекта
     * @return проект, который изменен {@link com.nlmk.evteev.tm.entity.Project}
     */
    public Project update(final Long id, final String name, final String description) {
        Project project = findById(id);
        if (project == null)
            return null;
        project.setName(name);
        project.setDescription(description);
        return project;
    }

}
